####################################################################
# IMPORT EXTERNAL LIBRARIES
import numpy as np
from scipy.integrate import odeint
from pyquaternion import Quaternion

reference_point = np.array([1, 0, 0])

####################################################################
####################################################################


class HeMoG():
    def __init__(self, parameters):
        # Initial state
        self.t = 0
        self.y = []
        self.pos = []

        # Parameters
        self.parameters = parameters
        self.frame_rate = parameters['fps']

        self.sphere_pts_idx = parameters['sphere_pts_idx']
        self.sphere_points = parameters['sphere_points']

    def next_location(self, salmap):
        '''
            Output:
                y = [velocity_quaternion, acceleration_quaternion]
        '''
        feature_maps = [salmap]

        self.y = compute_next_location(
            # Initial condition of the system and time instants to integrate
            times=np.arange(self.t, self.t + (1 / self.frame_rate) + .05, .1),
            y0=self.y,
            feature_maps=feature_maps,
            sphere_points=self.sphere_points,
            sphere_pts_idx=self.sphere_pts_idx,
            # System parameters
            parameters=self.parameters,
        )
        # Predicted point
        quaternion_zero = Quaternion(self.y[0], self.y[1], self.y[2], self.y[3])
        pred = quaternion_zero.rotate(reference_point)
        return pred

    def reset(self, y=[]):
        # Initial state
        self.t = 0
        self.y = y


####################################################################

def compute_next_location(
        # Initial condition of the system and
        # time instants to integrate
        times,
        y0,

        feature_maps,
        sphere_points,
        sphere_pts_idx,
        # System parameters
        parameters
):
    ''' Given input feature maps, this function returns the next velocity of the visual
        attention scanpath '''

    # If not provided, generate random initial conditions
    if len(y0) == 0:
        y0 = generate_initial_conditions()

    # Generate scanpath (by integrating diff. equations)
    y = odeint(myode, y0, times,
               args=(feature_maps, parameters, sphere_points, sphere_pts_idx),
               mxstep=100, rtol=.1, atol=.1
               )

    return list(y[-1])


####################################################################

def generate_initial_conditions():
    ''' This function generates initial condition for the dynamical system to be
    integrated. Numbers used here are arbitrary. Consider to modify or determine better
    numbers in future implementations. '''

    x = np.random.rand()
    y = np.random.rand()
    z = np.random.rand()
    r = np.linalg.norm([x, y, z])
    ang = np.random.rand() * np.pi

    return np.array([ang, x / r, y / r, z / r])


####################################################################

def myode(y, t, feature_maps, parameters, sphere_points, sphere_pts_idx):
    '''	This function describes the system of two second-order differential
        equations which describe visual attention. (VERSION 4 - GRAVITATIONAL 360)

        y: it is the vector of the variables (x1, x2, x3, dot x1, dot x2, dot x3)

        t: time (frames)

        parameters: dictionary containing all the parameters of the model '''

    dissipation = parameters['dissipation']
    alpha_sal = parameters['alpha_sal']
    moment_of_inertia = parameters['moment_of_inertia']

    salmap = feature_maps[0].astype(float)
    # if not salmap.max() == 0: salmap /= salmap.max()

    flat_salmap = np.reshape(salmap, [-1])
    salmap_crop = np.take(flat_salmap, sphere_pts_idx, axis=0)
    # if not salmap_crop.max() == 0: salmap_crop /= np.sum(salmap_crop)

    quaternion_zero = Quaternion(y[0], y[1], y[2], y[3])
    curr_point = quaternion_zero.rotate(reference_point)

    distances_sphere = create_distances_sphere(sphere_points, a=curr_point)

    SM_x = alpha_sal * np.array([
        (distances_sphere[:, 0] * salmap_crop).sum(),
        (distances_sphere[:, 1] * salmap_crop).sum(),
        (distances_sphere[:, 2] * salmap_crop).sum()
    ])

    "System of differential equations"

    F_x_a = SM_x

    quaternion_one = Quaternion(y[4], y[5], y[6], y[7])

    quaternion_vel = 2 * quaternion_one * quaternion_zero.inverse
    torque = (1 / moment_of_inertia) * (np.cross(curr_point, F_x_a) - dissipation * quaternion_vel.vector)
    quaternion_acc = Quaternion(0, torque[0], torque[1], torque[2])

    quaternion_two = (quaternion_one * quaternion_zero.inverse * quaternion_one) + ((0.5) * quaternion_acc * quaternion_zero)

    dy = [
        quaternion_one.w,  # Velocity quaternion w
        quaternion_one.x,  # Velocity quaternion x
        quaternion_one.y,  # Velocity quaternion y
        quaternion_one.z,  # Velocity quaternion z
        quaternion_two.w,  # Acceleration quaternion w
        quaternion_two.x,  # Acceleration quaternion x
        quaternion_two.y,  # Acceleration quaternion y
        quaternion_two.z   # Acceleration quaternion z
    ]

    return dy


####################################################################

def create_distances_sphere(x, a):
    '''
    :param x: is the positions of all points in the sphere (in 3d coordinates). Dimension Nx3, where N is the number of
    sampled points from the sphere.
    :param a: is the focus point (in 3d coordinates). Dimension 3x1
    :return: Create distances_mask for sum on the frame
            (x - a) / |x-a|**2
            notice: (x-a) is a vector.
            The resulting matrix is of dimension Nx3.
    '''
    diff_pt_pix = np.subtract(x, a)
    dist_to_pt = np.sqrt(np.square(diff_pt_pix[:, 0])+np.square(diff_pt_pix[:, 1])+np.square(diff_pt_pix[:,2]))
    dist_arr = np.divide(diff_pt_pix, np.square(dist_to_pt)[:, None])
    # To avoid division by zero
    dist_arr = np.nan_to_num(dist_arr)
    return dist_arr


####################################################################