import sys
from sklearn.preprocessing import normalize
import numpy as np
import os
import pandas as pd
import pickle
import warnings

warnings.filterwarnings("ignore")

DATASET_DIRECTORY = "sampled_dataset_0_2_Xu_CVPR_18"
SALIENCY_FOLDER = 'saliency_folder'
CB_SALIENCY_FOLDER = 'extract_saliency/saliency'
OPTICAL_FLOW_FOLDER = 'optical_flow'
VIDEO_CATEGORIES_FILE = 'video_categories.csv'

NUM_OF_POINTS_TO_SAMPLE_FROM_SPHERE = 10000

IMAGE_HEIGHT = 216
IMAGE_WIDTH = 384

init_window = 30
end_window = 25
m_window = 5
h_window = 25
attractiveness_window = 10

FRAMES_PER_SECOND = 5


def normalized(v):
    return normalize(v[:, np.newaxis], axis=0).ravel()


def cartesian_to_eulerian(x, y, z):
    latitude = np.arctan2(np.sqrt(x*x + y*y), z)
    # remainder is used to transform it in the positive range [0, 2*pi)
    longitude = np.remainder(np.arctan2(y, x), 2*np.pi)
    return longitude, latitude


def normalize_eulerian(longitude, latitude):
    norm_longitude = 1 - (longitude / (2*np.pi))
    norm_latitude = latitude / np.pi
    return norm_longitude, norm_latitude


def eulerian_to_cartesian(long, lat):
    x = np.sin(lat)*np.cos(long)
    y = np.sin(lat)*np.sin(long)
    z = np.cos(lat)
    return x, y, z


def load_dataset():
    dataset = {}
    for progress_video, video_id in enumerate(os.listdir(DATASET_DIRECTORY)):
        if os.path.isdir(os.path.join(DATASET_DIRECTORY, video_id)):
            dataset[video_id] = {}
            for fname in os.listdir(os.path.join(DATASET_DIRECTORY, video_id)):
                fname_base, fname_ext = os.path.splitext(fname)
                if fname_ext == '.csv':
                    user_id = fname_base.split('.')[0]
                    read_data = pd.read_csv(os.path.join(DATASET_DIRECTORY, video_id, fname))
                    dataset[video_id][user_id] = read_data
    return dataset


def load_normalized_cb_saliency(sampled_dataset, forTRACK=False):
    if forTRACK:
        from sklearn import preprocessing
        mmscaler = preprocessing.MinMaxScaler(feature_range=(-1, 1))
        normalized_saliencies = {}
        for enum_video, video in enumerate(sampled_dataset.keys()):
            loaded_sal = []
            with open(('%s/%s/%s' % (CB_SALIENCY_FOLDER, video, video)), 'rb') as f:
                u = pickle._Unpickler(f)
                u.encoding = 'latin1'
                p = u.load()
                for frame_id in range(1, len(p.keys()) + 1):
                    salmap = p['%03d' % frame_id]
                    salmap_norm = mmscaler.fit_transform(salmap.ravel().reshape(-1, 1)).reshape(salmap.shape)
                    loaded_sal.append(salmap_norm)
            normalized_saliencies[video] = np.array(loaded_sal)
        return normalized_saliencies
    else:
        normalized_saliencies = {}
        for enum_video, video in enumerate(sampled_dataset.keys()):
            loaded_sal = []
            with open(('%s/%s/%s' % (CB_SALIENCY_FOLDER, video, video)), 'rb') as f:
                u = pickle._Unpickler(f)
                u.encoding = 'latin1'
                p = u.load()
                for frame_id in range(1, len(p.keys())+1):
                    salmap = p['%03d' % frame_id]
                    loaded_sal.append(salmap)
            loaded_sal = np.array(loaded_sal)
            max_val = np.max(loaded_sal)
            min_val = np.min(loaded_sal)
            normalized_saliencies[video] = (loaded_sal - min_val) / (max_val - min_val)
        return normalized_saliencies


def cartesian_to_normalized_eulerian(x, y, z):
    longitude, latitude = cartesian_to_eulerian(x, y, z)
    longitude, latitude = normalize_eulerian(longitude, latitude)
    return longitude, latitude


def compute_orthodromic_distance(true_position, pred_position):
    norm_a = np.sqrt(np.square(true_position[0]) + np.square(true_position[1]) + np.square(true_position[2]))
    norm_b = np.sqrt(np.square(pred_position[0]) + np.square(pred_position[1]) + np.square(pred_position[2]))
    x_true = true_position[0] / norm_a
    y_true = true_position[1] / norm_a
    z_true = true_position[2] / norm_a
    x_pred = pred_position[0] / norm_b
    y_pred = pred_position[1] / norm_b
    z_pred = pred_position[2] / norm_b
    great_circle_distance = np.arccos(
        np.maximum(np.minimum(x_true * x_pred + y_true * y_pred + z_true * z_pred, 1.0), -1.0))
    return great_circle_distance

def get_of_map(video_id):
    optical_flow_frames_orig = np.load(os.path.join(OPTICAL_FLOW_FOLDER, '%s.npy' % video_id), allow_pickle=True)
    max_val = np.max(optical_flow_frames_orig)
    min_val = np.min(optical_flow_frames_orig)
    optical_flow_frames = (optical_flow_frames_orig - min_val) / (max_val - min_val)
    return optical_flow_frames

def get_vid_categories():
    categ_list = {}
    cat_vids_df = pd.read_csv(VIDEO_CATEGORIES_FILE, dtype='str')
    categ_list["Exploratory"] = [x for x in cat_vids_df['Exploratory'].values if str(x) != 'nan']
    categ_list["Moving Focus"] = [x for x in cat_vids_df['Moving Focus'].values if str(x) != 'nan']
    categ_list["Static Focus"] = [x for x in cat_vids_df['Static Focus'].values if str(x) != 'nan']
    categ_list["Rides"] = [x for x in cat_vids_df['Rides'].values if str(x) != 'nan']
    return categ_list

sampled_dataset = load_dataset()

partition_test = {
'003': ['p003', 'p006', 'p007', 'p011', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p034', 'p036', 'p041', 'p042'],
'013': ['p003', 'p006', 'p007', 'p011', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p034', 'p036', 'p041', 'p042'],
'018': ['p003', 'p006', 'p007', 'p011', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p034', 'p036', 'p041', 'p042'],
'021': ['p003', 'p006', 'p007', 'p011', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p034', 'p036', 'p041', 'p042'],
'026': ['p003', 'p006', 'p007', 'p011', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p034', 'p036', 'p041', 'p042'],
'032': ['p003', 'p006', 'p007', 'p011', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p034', 'p036', 'p041', 'p042'],
'035': ['p003', 'p006', 'p007', 'p011', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p034', 'p036', 'p041', 'p042'],
'039': ['p003', 'p006', 'p007', 'p011', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p034', 'p036', 'p041', 'p042'],
'048': ['p003', 'p006', 'p007', 'p011', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p034', 'p036', 'p041', 'p042'],
'056': ['p003', 'p006', 'p007', 'p011', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p034', 'p036', 'p041', 'p042'],
'066': ['p003', 'p006', 'p007', 'p011', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p034', 'p036', 'p041', 'p042'],
'068': ['p003', 'p006', 'p007', 'p011', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p034', 'p036', 'p041', 'p042'],
'073': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'085': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'093': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'096': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'103': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'110': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'112': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'113': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'120': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'123': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'130': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'136': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'140': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'142': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'143': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p020', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'146': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'150': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'154': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'156': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'159': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'161': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'164': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'167': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'168': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'170': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p022', 'p023', 'p026', 'p028', 'p030', 'p032', 'p036', 'p039', 'p041', 'p042'],
'182': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p022', 'p026', 'p030', 'p032', 'p036', 'p041', 'p042'],
'186': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p022', 'p026', 'p030', 'p032', 'p036', 'p041', 'p042'],
'197': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p022', 'p026', 'p030', 'p032', 'p036', 'p041', 'p042'],
'198': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p022', 'p026', 'p030', 'p032', 'p036', 'p041', 'p042'],
'210': ['p003', 'p006', 'p007', 'p011', 'p013', 'p014', 'p016', 'p018', 'p021', 'p022', 'p026', 'p030', 'p032', 'p036', 'p041', 'p042']
}

gd_saliency_map = load_normalized_cb_saliency(partition_test, forTRACK=True)

vid_categ = get_vid_categories()

#######################################################
avege_error = {}
explo_error = {}
movnF_error = {}
statF_error = {}
rides_error = {}
for tstep in range(h_window):
    avege_error[tstep] = []
    explo_error[tstep] = []
    movnF_error[tstep] = []
    statF_error[tstep] = []
    rides_error[tstep] = []

for enumerate_vid_counter, video_id in enumerate(partition_test.keys()):

    text_loading = "Getting results... %.2f%% " % ((enumerate_vid_counter / len(partition_test.keys())) * 100)
    sys.stdout.write('\r' + text_loading)

    error_per_video_per_tstep_geymol = {}
    for tstep in range(h_window):
        error_per_video_per_tstep_geymol[tstep] = []

    optical_flow_frames = get_of_map(video_id)

    for user_id in partition_test[video_id]:

        trace_length = min(len(gd_saliency_map[video_id]), len(sampled_dataset[video_id][user_id]['x_head'].values), len(optical_flow_frames))

        for tstamp in range(init_window, trace_length - end_window, h_window):

            history_x = sampled_dataset[video_id][user_id]['x_head'][tstamp-m_window:tstamp+1]
            history_y = sampled_dataset[video_id][user_id]['y_head'][tstamp-m_window:tstamp+1]
            history_z = sampled_dataset[video_id][user_id]['z_head'][tstamp-m_window:tstamp+1]

            pos_history_input = np.stack((history_x, history_y, history_z), axis=1)

            no_motion_prediction = pos_history_input[-1]

            for tstep in range(h_window):

                pred = no_motion_prediction
                norm_factor = np.sqrt(pred[0] * pred[0] + pred[1] * pred[1] + pred[2] * pred[2])
                pos_only_pred = pred / norm_factor

                groundtruth = np.array([sampled_dataset[video_id][user_id]['x_head'][tstamp + tstep + 1],
                                        sampled_dataset[video_id][user_id]['y_head'][tstamp + tstep + 1],
                                        sampled_dataset[video_id][user_id]['z_head'][tstamp + tstep + 1]])

                error_for_tstep = compute_orthodromic_distance(groundtruth, pos_only_pred)
                error_per_video_per_tstep_geymol[tstep].append(error_for_tstep)

    for tstep in range(h_window):
        tst_avg_err = np.mean(error_per_video_per_tstep_geymol[tstep])
        avege_error[tstep].append(tst_avg_err)
        if video_id in vid_categ['Exploratory']:
            explo_error[tstep].append(tst_avg_err)
        elif video_id in vid_categ['Moving Focus']:
            movnF_error[tstep].append(tst_avg_err)
        elif video_id in vid_categ['Static Focus']:
            statF_error[tstep].append(tst_avg_err)
        elif video_id in vid_categ['Rides']:
            rides_error[tstep].append(tst_avg_err)

print('\nTstamp \t Average \t Exploratory \t Moving Focus \t Static Focus \t Rides')
for tstep in range(h_window):
    print('%.2f \t %.3f \t\t %.3f \t\t\t %.3f \t\t\t %.3f \t\t\t %.3f' % (
    (tstep + 1) * 0.2, np.mean(avege_error[tstep]), np.mean(explo_error[tstep]), np.mean(movnF_error[tstep]),
    np.mean(statF_error[tstep]), np.mean(rides_error[tstep])))