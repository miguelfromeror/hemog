####################################################################
# IMPORT EXTERNAL LIBRARIES
import numpy as np
from scipy.integrate import odeint
from pyquaternion import Quaternion

reference_point = np.array([1, 0, 0])

####################################################################
####################################################################


class HeMoG():
    def __init__(self, parameters):
        # Initial state
        self.t = 0
        self.y = []
        self.pos = []

        # Parameters
        self.parameters = parameters
        self.frame_rate = parameters['fps']

    def next_location(self, tstep):
        '''
            Output:
                y = [velocity_quaternion, acceleration_quaternion]
        '''
        self.y = compute_next_location(
            # Initial condition of the system and time instants to integrate
            y0=self.y,
            times=np.arange(self.t, self.t + (1 / self.frame_rate) + .05, .1),
            # System parameters
            parameters=self.parameters,
            tstep=tstep
        )
        # Predicted point
        quaternion_zero = Quaternion(self.y[0], self.y[1], self.y[2], self.y[3])
        pred = quaternion_zero.rotate(reference_point)
        return pred

    def reset(self, y=[]):
        # Initial state
        self.t = 0
        self.y = y


####################################################################

def compute_next_location(
        # Initial condition of the system and
        # time instants to integrate
        times,
        y0,

        # System parameters
        parameters,
        tstep
):
    ''' Given input feature maps, this function returns the next velocity of the visual
        attention scanpath '''

    # If not provided, generate random initial conditions
    if len(y0) == 0:
        y0 = generate_initial_conditions()

    # Generate scanpath (by integrating diff. equations)
    y = odeint(myode, y0, times,
               args=(parameters, tstep),
               mxstep=100, rtol=.1, atol=.1
               )

    return list(y[-1])


####################################################################

def generate_initial_conditions():
    ''' This function generates initial condition for the dynamical system to be
    integrated. Numbers used here are arbitrary. Consider to modify or determine better
    numbers in future implementations. '''

    x = np.random.rand()
    y = np.random.rand()
    z = np.random.rand()
    r = np.linalg.norm([x, y, z])
    ang = np.random.rand() * np.pi

    return np.array([ang, x / r, y / r, z / r])


####################################################################

def myode(y, t, parameters, tstep):
    '''	This function describes the system of two second-order differential
        equations which describe visual attention. (VERSION 4 - GRAVITATIONAL 360)

        y: it is the vector of the variables (x1, x2, x3, dot x1, dot x2, dot x3)

        t: time (frames)

        parameters: dictionary containing all the parameters of the model '''

    dissipation = parameters['dissipation']
    moment_of_inertia = parameters['moment_of_inertia']

    "System of differential equations"

    quaternion_zero = Quaternion(y[0], y[1], y[2], y[3])
    quaternion_one = Quaternion(y[4], y[5], y[6], y[7])
    quaternion_vel = 2 * quaternion_one * quaternion_zero.inverse
    torque = (1 / moment_of_inertia) * (np.array([0, 0, 0])-dissipation*quaternion_vel.vector)
    quaternion_acc = Quaternion(0, torque[0], torque[1], torque[2])
    quaternion_two = (quaternion_one * quaternion_zero.inverse * quaternion_one) + ((0.5) * quaternion_acc * quaternion_zero)

    dy = [
        quaternion_one.w,  # Velocity quaternion w
        quaternion_one.x,  # Velocity quaternion x
        quaternion_one.y,  # Velocity quaternion y
        quaternion_one.z,  # Velocity quaternion z
        quaternion_two.w,  # Acceleration quaternion w
        quaternion_two.x,  # Acceleration quaternion x
        quaternion_two.y,  # Acceleration quaternion y
        quaternion_two.z   # Acceleration quaternion z
    ]

    return dy


####################################################################