import ast
from sklearn.preprocessing import normalize
import numpy as np
import os
import pandas as pd
import warnings
from ICC17_model import create_ICC17_model
import sys

warnings.filterwarnings("ignore")

DATASET_DIRECTORY = "sampled_dataset_0_2_Xu_CVPR_18"
SALIENCY_FOLDER = 'Xu_CVPR_18/true_saliency'

OPTICAL_FLOW_FOLDER = 'optical_flow'
VIDEO_CATEGORIES_FILE = 'video_categories.csv'

init_window = 30
end_window = 25
m_window = 5
h_window = 25

def normalized(v):
    return normalize(v[:, np.newaxis], axis=0).ravel()


def cartesian_to_eulerian(x, y, z):
    latitude = np.arctan2(np.sqrt(x*x + y*y), z)
    # remainder is used to transform it in the positive range [0, 2*pi)
    longitude = np.remainder(np.arctan2(y, x), 2*np.pi)
    return longitude, latitude


def normalize_eulerian(longitude, latitude):
    norm_longitude = 1 - (longitude / (2*np.pi))
    norm_latitude = latitude / np.pi
    return norm_longitude, norm_latitude


def eulerian_to_cartesian(long, lat):
    x = np.sin(lat)*np.cos(long)
    y = np.sin(lat)*np.sin(long)
    z = np.cos(lat)
    return x, y, z


def load_dataset():
    dataset = {}
    for progress_video, video_id in enumerate(os.listdir(DATASET_DIRECTORY)):
        # print('Loading progress: %0.3f%%' % (((progress_video / len(os.listdir(DATASET_DIRECTORY))) * 100)))
        if os.path.isdir(os.path.join(DATASET_DIRECTORY, video_id)):
            dataset[video_id] = {}
            for fname in os.listdir(os.path.join(DATASET_DIRECTORY, video_id)):
                fname_base, fname_ext = os.path.splitext(fname)
                if fname_ext == '.csv':
                    user_id = fname_base.split('.')[0]
                    dataset[video_id][user_id] = pd.read_csv(os.path.join(DATASET_DIRECTORY, video_id, fname))
    return dataset

def load_true_saliency(sampled_dataset):
    sal_for_all_videos = {}
    for enum_video, video in enumerate(sampled_dataset.keys()):
        saliencies_for_video_file = os.path.join(SALIENCY_FOLDER, video + '.npy')
        sal_for_all_videos[video] = np.load(saliencies_for_video_file)
    return sal_for_all_videos


def cartesian_to_normalized_eulerian(x, y, z):
    longitude, latitude = cartesian_to_eulerian(x, y, z)
    longitude, latitude = normalize_eulerian(longitude, latitude)
    return longitude, latitude


def compute_orthodromic_distance(true_position, pred_position):
    norm_a = np.sqrt(np.square(true_position[0]) + np.square(true_position[1]) + np.square(true_position[2]))
    norm_b = np.sqrt(np.square(pred_position[0]) + np.square(pred_position[1]) + np.square(pred_position[2]))
    x_true = true_position[0] / norm_a
    y_true = true_position[1] / norm_a
    z_true = true_position[2] / norm_a
    x_pred = pred_position[0] / norm_b
    y_pred = pred_position[1] / norm_b
    z_pred = pred_position[2] / norm_b
    great_circle_distance = np.arccos(
        np.maximum(np.minimum(x_true * x_pred + y_true * y_pred + z_true * z_pred, 1.0), -1.0))
    return great_circle_distance

def get_of_map(video_id):
    optical_flow_frames_orig = np.load(os.path.join(OPTICAL_FLOW_FOLDER, '%s.npy' % video_id), allow_pickle=True)
    max_val = np.max(optical_flow_frames_orig)
    min_val = np.min(optical_flow_frames_orig)
    optical_flow_frames = (optical_flow_frames_orig - min_val) / (max_val - min_val)
    return optical_flow_frames

def get_vid_categories():
    categ_list = {}
    cat_vids_df = pd.read_csv(VIDEO_CATEGORIES_FILE, dtype='str')
    categ_list["Exploratory"] = [x for x in cat_vids_df['Exploratory'].values if str(x) != 'nan']
    categ_list["Moving Focus"] = [x for x in cat_vids_df['Moving Focus'].values if str(x) != 'nan']
    categ_list["Static Focus"] = [x for x in cat_vids_df['Static Focus'].values if str(x) != 'nan']
    categ_list["Rides"] = [x for x in cat_vids_df['Rides'].values if str(x) != 'nan']
    return categ_list


sampled_dataset = load_dataset()

partition_test_file = open("partition_test.txt")
partition_test_content = partition_test_file.read()
partition_test = ast.literal_eval(partition_test_content)
partition_test_file.close()

dataset_pred_pos = {}

gd_saliency_map = load_true_saliency(partition_test)

TRACK_model = create_ICC17_model(m_window, h_window, 256, 256)
TRACK_model.load_weights('weights_TRACK_GTSal.hdf5')

vid_categ = get_vid_categories()

#######################################################
avege_error = {}
explo_error = {}
movnF_error = {}
statF_error = {}
rides_error = {}
for tstep in range(h_window):
    avege_error[tstep] = []
    explo_error[tstep] = []
    movnF_error[tstep] = []
    statF_error[tstep] = []
    rides_error[tstep] = []

for enumerate_vid_counter, video_id in enumerate(partition_test.keys()):

    text_loading = "Getting results... %.2f%% " % ((enumerate_vid_counter / len(partition_test.keys())) * 100)
    sys.stdout.write('\r' + text_loading)

    error_per_video_per_tstep_geymol = {}
    for tstep in range(h_window):
        error_per_video_per_tstep_geymol[tstep] = []

    dataset_pred_pos[video_id] = {}
    optical_flow_frames = get_of_map(video_id)

    for user_id in partition_test[video_id]:
        # print(user_id)
        dataset_pred_pos[video_id][user_id] = {}

        trace_length = min(len(gd_saliency_map[video_id]), len(sampled_dataset[video_id][user_id]['x_head'].values), len(optical_flow_frames))

        for tstamp in range(init_window, trace_length - end_window, h_window):

            history_x = sampled_dataset[video_id][user_id]['x_head'][tstamp-m_window:tstamp+1]
            history_y = sampled_dataset[video_id][user_id]['y_head'][tstamp-m_window:tstamp+1]
            history_z = sampled_dataset[video_id][user_id]['z_head'][tstamp-m_window:tstamp+1]

            track_pos_history_input = np.stack((history_x, -history_y, history_z), axis=1)

            TRACK_predictions = TRACK_model.predict([
                np.array([track_pos_history_input[:-1]]),
                np.array([gd_saliency_map[video_id][tstamp - m_window + 1:tstamp + 1]]),
                np.array([track_pos_history_input[-1:]]),
                np.array([gd_saliency_map[video_id][tstamp + 1:tstamp + h_window + 1]])
            ])

            path_sgeymol_rows = []
            path_sgeymol_cols = []

            for tstep in range(h_window):
                groundtruth = np.array([sampled_dataset[video_id][user_id]['x_head'][tstamp + tstep + 1],
                                        sampled_dataset[video_id][user_id]['y_head'][tstamp + tstep + 1],
                                        sampled_dataset[video_id][user_id]['z_head'][tstamp + tstep + 1]])

                pred_track = TRACK_predictions[0, tstep]
                pred_track = np.array([pred_track[0], -pred_track[1], pred_track[2]])
                TRACK_pred = pred_track / np.sqrt(pred_track[0] * pred_track[0] + pred_track[1] * pred_track[1] + pred_track[2] * pred_track[2])

                error_for_tstep = compute_orthodromic_distance(groundtruth, TRACK_pred)

                error_per_video_per_tstep_geymol[tstep].append(error_for_tstep)

    for tstep in range(h_window):
        tst_avg_err = np.mean(error_per_video_per_tstep_geymol[tstep])
        avege_error[tstep].append(tst_avg_err)
        if video_id in vid_categ['Exploratory']:
            explo_error[tstep].append(tst_avg_err)
        elif video_id in vid_categ['Moving Focus']:
            movnF_error[tstep].append(tst_avg_err)
        elif video_id in vid_categ['Static Focus']:
            statF_error[tstep].append(tst_avg_err)
        elif video_id in vid_categ['Rides']:
            rides_error[tstep].append(tst_avg_err)

print('\nTstamp \t Average \t Exploratory \t Moving Focus \t Static Focus \t Rides')
for tstep in range(h_window):
    print('%.2f \t %.3f \t\t %.3f \t\t\t %.3f \t\t\t %.3f \t\t\t %.3f' % (
        (tstep + 1) * 0.2, np.mean(avege_error[tstep]), np.mean(explo_error[tstep]),
        np.mean(movnF_error[tstep]),
        np.mean(statF_error[tstep]), np.mean(rides_error[tstep])))
