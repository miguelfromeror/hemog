# HeMoG

Gravitational white-box model for head motion estimation in 360° videos.

The interactive colab version is available at: [https://bit.ly/2Yj7Xsh](https://colab.research.google.com/drive/13NxJAk6bt6qo-o20SdnKeL6Z7TX9dG2C#scrollTo=LQEwgwmr-FCY).

## Requirements:
- Python 3.7
- pandas 1.3.2
- numpy 1.21.2
- scikit-learn 0.24.2
- matplotlib 3.4.3
- pyquaternion 0.9.9
- imageio 2.9.0
- opencv-python 4.5.3.56
- keras 2.6.0
- tensorflow-gpu 2.6.0


## Data:

### Dataset:
The dataset used is the one from [1]: https://pan.baidu.com/s/1lGNmttCTs835_8lQ8YN2-g#list/path=%2F

### Saliency files and other relevant information:
The content-base saliency maps (Xu_CVPR_18/extract_saliency) extracted from PanoSalNet [2] can be downloaded from the following link:
https://unice-my.sharepoint.com/:f:/g/personal/miguel_romero-rondon_unice_fr/EttMwMOG-LxFku_qU8qYKK8BW363wQT8jDujn1bpMd2PqA?e=7gevwF

The ground-truth saliency maps, the optical flow maps and the object detection frames are available at:
https://unice-my.sharepoint.com/:f:/g/personal/miguel_romero-rondon_unice_fr/EqscTF6X7elFu-bVsyFpzTcBlycyksYDn-bm_P_gWYXn-Q?e=3AYRR3

## Usage

This repository contains several examples of use, in general the way to use HeMoG is:

    params = {'dissipation': 2.5, 'fps': 30, 'moment_of_inertia': 1, 'sphere_points': [3D points sampled from the sphere using VOGEL method], 'sphere_pts_idx': [1D indices of the sampled points]}
    foa = HeMoG(params)
    foa.y = [position_quaternion, velocity_quaternion]
    foa.pos = pos_0
    
    for t in range(T):
        foa.parameters['alpha_sal'] = 1-np.exp(-0.1*t)
        hemog_pred_t = foa.next_location(salmap_t)

## Results

To get the results of the figures in the paper run the following commands:

Figure 3:
* `Results_HeMoG_NoVisualFeatures.py -lambda 2.5`
* `Results_HeMoG_NoVisualFeatures.py -lambda 0.5`
* `Results_HeMoG_NoVisualFeatures.py -lambda 0.05`
* `python Results_PosOnly.py`

Figure 4:
* `Results_HeMoG_GTSal.py -beta 0.1`
* `Results_HeMoG_GTSal.py -beta 0.01`
* `Results_HeMoG_GTSal.py -beta 0.001`
* `Results_TRACK_GTSal.py`

Figure 6:
* `Results_TRACK_CBSal_MovObj.py`
* `Results_TRACK_GTSal.py`
* `Results_HeMoG_CBSal_MovObj.py -beta 0.001`
* `Results_HeMoG_CBSal_MovObj.py -beta 0.0001`
* `Results_HeMoG_CBSal_MovObj.py -beta 0.00001`
* `Results_HeMoG_GTSal.py -beta 0.1`

Figure 8:
* `Results_TRACK_CBSal_MovObj.py`
* `Results_TRACK_GTSal.py`
* `Results_HeMoG_CBSal_MovObj.py -beta 0.001`
* `Results_HeMoG_CBSal_MovObj.py -beta 0.0001`
* `Results_HeMoG_CBSal_MovObj.py -beta 0.00001`

Figure 9:
* `Results_TRACK_CBSal_MovObj.py`
* `Results_TRACK_GTSal.py`
* `Results_HeMoG_CBSal_MovObj.py -bestbeta`
* `Results_HeMoG_GTSal.py -beta 0.001`

Figure 10:
* `Results_TRACK_CBSal_PanoSalNet.py`
* `Results_TRACK_GTSal.py`
* `Results_HeMoG_CBSal_PanoSalNet.py -beta 0.001`
* `Results_HeMoG_CBSal_PanoSalNet.py -beta 0.0001`
* `Results_HeMoG_CBSal_PanoSalNet.py -beta 0.00001`
* `Results_HeMoG_GTSal.py -beta 0.001`

### References

[1] Xu, Yanyu, Yanbing Dong, Junru Wu, Zhengzhong Sun, Zhiru Shi, Jingyi Yu, and Shenghua Gao. "Gaze prediction in dynamic 360 immersive videos." In proceedings of the IEEE Conference on Computer Vision and Pattern Recognition, pp. 5333-5342. 2018.

[2] Anh Nguyen, Zhisheng Yan, and Klara Nahrstedt 2018. Your Attention is Unique: Detecting 360-Degree Video Saliency in Head-Mounted Display for Head Movement Prediction. In ACM Multimedia Conference for 2018 (ACMMM2018)
