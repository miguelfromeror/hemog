from keras.models import Model
from keras.layers import Dense, LSTM, Lambda, Input, Reshape, TimeDistributed, Concatenate, Flatten
from keras import optimizers
from keras import backend as K
import tensorflow as tf

def metric_orth_dist(position_a, position_b):
    # Transform into directional vector in Cartesian Coordinate System
    norm_a = tf.sqrt(tf.square(position_a[:, :, 0:1]) + tf.square(position_a[:, :, 1:2]) + tf.square(position_a[:, :, 2:3]))
    norm_b = tf.sqrt(tf.square(position_b[:, :, 0:1]) + tf.square(position_b[:, :, 1:2]) + tf.square(position_b[:, :, 2:3]))
    x_true = position_a[:, :, 0:1]/norm_a
    y_true = position_a[:, :, 1:2]/norm_a
    z_true = position_a[:, :, 2:3]/norm_a
    x_pred = position_b[:, :, 0:1]/norm_b
    y_pred = position_b[:, :, 1:2]/norm_b
    z_pred = position_b[:, :, 2:3]/norm_b
    # Finally compute orthodromic distance
    # great_circle_distance = np.arccos(x_true*x_pred+y_true*y_pred+z_true*z_pred)
    # To keep the values in bound between -1 and 1
    great_circle_distance = tf.acos(tf.maximum(tf.minimum(x_true * x_pred + y_true * y_pred + z_true * z_pred, 1.0), -1.0))
    return great_circle_distance

# This way we ensure that the network learns to predict the delta angle
def toPosition(values):
    orientation = values[0]
    # The network returns values between 0 and 1, we force it to be between -1/2 and 1/2
    motion = values[1]
    return (orientation + motion)

def selectImageInModel(input_to_selector, curr_idx):
    selected_image = input_to_selector[:, curr_idx:curr_idx+1]
    return selected_image

def add_timestep_axis(input):
    return tf.expand_dims(input, 1)

# ----------------------------- TRAIN ----------------------------
def create_ICC17_model(M_WINDOW, H_WINDOW, NUM_TILES_HEIGHT, NUM_TILES_WIDTH):
    # Defining model structure
    encoder_position_inputs = Input(shape=(M_WINDOW, 3))
    encoder_saliency_inputs = Input(shape=(M_WINDOW, NUM_TILES_HEIGHT, NUM_TILES_WIDTH, 1))
    decoder_position_inputs = Input(shape=(1, 3))
    decoder_saliency_inputs = Input(shape=(H_WINDOW, NUM_TILES_HEIGHT, NUM_TILES_WIDTH, 1))

    sense_pos_enc = LSTM(units=256, return_sequences=True, return_state=True, name='prop_lstm_1_enc')

    sense_sal_enc = LSTM(units=256, return_sequences=True, return_state=True, name='prop_lstm_2_enc')

    fuse_1_enc = LSTM(units=256, return_sequences=True, return_state=True)

    sense_pos_dec = LSTM(units=256, return_sequences=True, return_state=True, name='prop_lstm_1_dec')

    sense_sal_dec = LSTM(units=256, return_sequences=True, return_state=True, name='prop_lstm_2_dec')

    fuse_1_dec = LSTM(units=256, return_sequences=True, return_state=True)

    fuse_2 = Dense(units=256)

    # Act stack
    fc_layer_out = Dense(3)
    To_Position = Lambda(toPosition)

    # Encoding
    out_enc_pos, state_h_1, state_c_1 = sense_pos_enc(encoder_position_inputs)
    states_1 = [state_h_1, state_c_1]

    out_flat_enc = TimeDistributed(Flatten())(encoder_saliency_inputs)
    out_enc_sal, state_h_2, state_c_2 = sense_sal_enc(out_flat_enc)
    states_2 = [state_h_2, state_c_2]

    conc_out_enc = Concatenate(axis=-1)([out_enc_sal, out_enc_pos])

    fuse_out_enc, state_h_fuse, state_c_fuse = fuse_1_enc(conc_out_enc)
    states_fuse = [state_h_fuse, state_c_fuse]

    # Decoding
    all_pos_outputs = []
    inputs = decoder_position_inputs
    for curr_idx in range(H_WINDOW):
        out_enc_pos, state_h_1, state_c_1 = sense_pos_dec(inputs, initial_state=states_1)
        states_1 = [state_h_1, state_c_1]

        selected_timestep_saliency = Lambda(selectImageInModel, arguments={'curr_idx': curr_idx})(decoder_saliency_inputs)
        flatten_timestep_saliency = Reshape((1, NUM_TILES_WIDTH * NUM_TILES_HEIGHT))(selected_timestep_saliency)
        out_enc_sal, state_h_2, state_c_2 = sense_sal_dec(flatten_timestep_saliency, initial_state=states_2)
        states_2 = [state_h_2, state_c_2]

        conc_out_dec = Concatenate(axis=-1)([out_enc_sal, out_enc_pos])

        fuse_out_dec_1, state_h_fuse, state_c_fuse = fuse_1_dec(conc_out_dec, initial_state=states_fuse)
        # ToDo Remember to use this
        # states_fuse = [state_h_fuse, state_c_fuse]
        fuse_out_dec_2 = TimeDistributed(fuse_2)(fuse_out_dec_1)

        outputs_delta = fc_layer_out(fuse_out_dec_2)

        decoder_pred = To_Position([inputs, outputs_delta])

        all_pos_outputs.append(decoder_pred)
        # Reinject the outputs as inputs for the next loop iteration as well as update the states
        inputs = decoder_pred

    # Concatenate all predictions
    decoder_outputs_pos = Lambda(lambda x: K.concatenate(x, axis=1))(all_pos_outputs)

    # Define and compile model
    model = Model([encoder_position_inputs, encoder_saliency_inputs, decoder_position_inputs, decoder_saliency_inputs], decoder_outputs_pos)
    return model
